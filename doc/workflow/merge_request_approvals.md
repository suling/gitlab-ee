---
redirect_to: '../user/project/merge_requests/merge_request_approvals.md'
---

This document was moved to [user/project/merge_requests/merge_request_approvals](../user/project/merge_requests/merge_request_approvals.md).
